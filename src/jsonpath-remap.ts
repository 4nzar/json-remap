import * as jsonpath from "jsonpath";

const seekString = (data: any, schema: any, result: any, key?: string | number): void => {
	if (key !== undefined) {
		if (schema.indexOf('$.') < 0)
			result[key] = schema;
		else {
			const items: any = jsonpath.query(data, schema);
			result[key] = items.length ? items[0] : undefined;
		}
	}
}

const seekArray = (data: any, schema: any, result: any, key?: string | number): void => {
	if (key !== undefined) {
		if (schema.length == 2) {
			const sub: any = schema[1];
			const path: any = schema[0];
			const items: any = jsonpath.query(data, path) || [];
			if (items.length && sub) {
				result = result[key] = [];
				for (let i = 0; i < items.length; i++)
					seek(items[i], sub, result, i);
			}
			else
				result[key] = items;
		}
		else
			result[key] = schema;
	}
};

const seek = (data: any, schema: any, result: any, key?: string | number): void => {
	if (key !== undefined)
		result = result[key] = {};
	for (let k in schema) {
		let action: Function | null = null;
		let path: any = schema[k];
		let type: string = typeof path;
		if (type == "object")
			action = (Array.isArray(path)) ? seekArray : seek;
		else if (type == "string")
			action = seekString;
		else
			result[k] = path;
		if (action)
			action(data, path, result, k);
	}
};

export const transform = (data: any, schema: any): any => {
	let result: any = {};
	seek(data, schema, result);
	return (result);
};

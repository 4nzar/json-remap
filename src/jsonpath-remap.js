import * as jsonpath from "jsonpath";
const seekString = (data, schema, result, key) => {
    if (key !== undefined) {
        if (schema.indexOf('$.') < 0)
            result[key] = schema;
        else {
            const items = jsonpath.query(data, schema);
            result[key] = items.length ? items[0] : undefined;
        }
    }
};
const seekArray = (data, schema, result, key) => {
    if (key !== undefined) {
        if (schema.length == 2) {
            const sub = schema[1];
            const path = schema[0];
            const items = jsonpath.query(data, path) || [];
            if (items.length && sub) {
                result = result[key] = [];
                for (let i = 0; i < items.length; i++)
                    seek(items[i], sub, result, i);
            }
            else
                result[key] = items;
        }
        else
            result[key] = schema;
    }
};
const seek = (data, schema, result, key) => {
    if (key !== undefined)
        result = result[key] = {};
    for (let k in schema) {
        let action = null;
        let path = schema[k];
        let type = typeof path;
        if (type == "object")
            action = (Array.isArray(path)) ? seekArray : seek;
        else if (type == "string")
            action = seekString;
        else
            result[k] = path;
        if (action)
            action(data, path, result, k);
    }
};
export const transform = (data, schema) => {
    let result = {};
    seek(data, schema, result);
    return (result);
};
//# sourceMappingURL=jsonpath-remap.js.map